package cn.spream.jstudy.activemq.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import cn.spream.jstudy.activemq.User;

import java.util.Map;

/**
 * User类型消息处理器
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:22
 * To change this template use File | Settings | File Templates.
 */
public class UserProcessor implements Processor {

    private static final Log log = LogFactory.getLog(UserProcessor.class);

    @Override
    public void process(Map<String, ? extends Object> params) {
        log.info("UserProcessor开始处理MQ消息");
        User user = (User) params.get("user");
        log.info(user);
    }

}

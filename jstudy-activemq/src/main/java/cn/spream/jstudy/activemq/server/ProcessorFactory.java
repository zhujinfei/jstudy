package cn.spream.jstudy.activemq.server;

import cn.spream.jstudy.activemq.MQType;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:44
 * To change this template use File | Settings | File Templates.
 */
public class ProcessorFactory {

    private static Processor userProcessor;

    public static Processor getProcessor(MQType mqType){
        Processor processor = null;
        switch (mqType){
            case USER:
                processor = userProcessor;
                break;
            default:
                throw new RuntimeException("没有对应的处理者，mqType:" + mqType);
        }
        return processor;
    }

    public Processor getUserProcessor() {
        return userProcessor;
    }

    public void setUserProcessor(Processor userProcessor) {
        this.userProcessor = userProcessor;
    }
}

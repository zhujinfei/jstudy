package cn.spream.jstudy.activemq.server;

import java.util.Map;

/**
 * 处理器接口
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:24
 * To change this template use File | Settings | File Templates.
 */
public interface Processor {

    public void process(Map<String, ? extends Object> params);

}

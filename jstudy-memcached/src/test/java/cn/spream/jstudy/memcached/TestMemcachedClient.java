package cn.spream.jstudy.memcached;

import net.spy.memcached.MemcachedClient;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-14
 * Time: 下午9:45
 * To change this template use File | Settings | File Templates.
 */
public class TestMemcachedClient {

    @Test
    public void testMemcachedClient(){
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        MemcachedClient memcachedClient = (MemcachedClient) context.getBean("memcachedClient");

        List<String> list1 = new ArrayList<String>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        list1.add("d");
        list1.add("e");
        memcachedClient.add("list", 10000, list1);

        String str1 = "test";
        memcachedClient.add("str", 10000, str1);

        List<String> list2 = (List<String>) memcachedClient.get("list");
        for(String s : list2){
            System.out.println(s);
        }

        String str2 = (String) memcachedClient.get("str");
        System.out.println(str2);
    }

}

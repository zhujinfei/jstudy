package cn.spream.jstudy.crypto;

import org.junit.Test;

/**
 * 测试DES加密
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-11-12
 * Time: 下午6:06
 * To change this template use File | Settings | File Templates.
 */
public class TestDESCrypto {

    @Test
    public void test() throws Exception {
        String key = "12345678123456781234567812345678";
        String source = "hello des";
        String encrypt = DESCrypto.encrypt(key, source);
        String decrypt = DESCrypto.decrypt(key, encrypt);
        System.out.println("原始数据：" + source);
        System.out.println("加密结果：" + encrypt);
        System.out.println("解密结果：" + decrypt);
    }

}

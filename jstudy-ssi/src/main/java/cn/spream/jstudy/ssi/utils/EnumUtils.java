package cn.spream.jstudy.ssi.utils;

import cn.spream.jstudy.ssi.domain.enums.SexEnum;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-31
 * Time: 下午4:59
 * To change this template use File | Settings | File Templates.
 */
public class EnumUtils {

    public String getSexValueByKey(int key){
        return SexEnum.getValueByKey(key);
    }

}

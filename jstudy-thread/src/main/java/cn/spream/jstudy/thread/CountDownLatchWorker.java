package cn.spream.jstudy.thread;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-1
 * Time: 下午3:37
 * To change this template use File | Settings | File Templates.
 */
public class CountDownLatchWorker extends Thread {

    private String name;
    private CountDownLatch countDownLatch;
    private Random random = new Random();

    public CountDownLatchWorker(String name, CountDownLatch countDownLatch) {
        this.name = name;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            System.out.println(String.format("[%s]线程开始工作", name));
            long time = random.nextInt(5) * 1000;
            Thread.sleep(time);
            System.out.println(String.format("[%s]线程结束工作，共执行%d毫秒", name, time));
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally{
            countDownLatch.countDown();
        }
    }

}
